### Keiba statis odds

#### Pre require

- `node`
- `git`

#### Clone repository

- run: `git clone https://gitlab-new.bap.jp/dongvd/keiba-static-odds.git`

#### Create file environment

- run: `cp .env.example .env`
- update info file your .env

#### Install dependencies

- run: `npm install`

#### Run app

- run: `npm start`

DONE!!!
