FROM node:10.16.3-alpine

ENV NODE_ENV=production

WORKDIR /usr/src/app

COPY ["package.json", "./"]

RUN yarn

COPY . .

EXPOSE 4000

CMD ["yarn", "start"]
