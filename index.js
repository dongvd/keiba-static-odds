require("dotenv").config();

const express = require("express");
const path = require("path");
const app = express();

function auth(req, res, next) {
  const key = req.headers["authorization"];
  if (key !== process.env.SECRET_KEY) {
    return res.status(403).json({
      message: "Access Denied"
    });
  }
  return next();
}

app.use(auth, express.static(path.resolve(__dirname, "./data")));

app.use(function(req, res) {
  res.status(404).json({
    message: "Not Found"
  });
});

app.listen(4000, function() {
  console.info("Keiba static odds is running on port: 4000");
});
